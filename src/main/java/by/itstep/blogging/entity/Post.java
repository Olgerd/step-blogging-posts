package by.itstep.blogging.entity;

import java.util.Objects;

public class Post {

    private int postId;
    private String title;
    private String description;
    private int rating;
    private String authorName;

    public Post() {
    }

    public Post(int postId, String title, String description, int rating, String authorName) {
        this.postId = postId;
        this.title = title;
        this.description = description;
        this.rating = rating;
        this.authorName = authorName;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Post post = (Post) o;
        return postId == post.postId && rating == post.rating && Objects.equals(title, post.title) && Objects.equals(description, post.description) && Objects.equals(authorName, post.authorName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(postId, title, description, rating, authorName);
    }

    @Override
    public String toString() {
        return "Post{" +
                "postId=" + postId +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", rating=" + rating +
                ", authorName='" + authorName + '\'' +
                '}';
    }
}
