package by.itstep.blogging.repository;

import by.itstep.blogging.entity.Comment;
import by.itstep.blogging.entity.Post;
import by.itstep.blogging.utils.ConnectionUtils;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CommentRepository {

    private Connection con;
    private PreparedStatement st;
    private ResultSet rs;



    private static  final String INSERT_QUERY = "INSERT INTO comment(comment_id,post_id,message,author_name, rating, " +
      "created_at) VALUES (?,?,?,?,?,?);";
    private static  final String FIND_BY_ID_QUERY = "SELECT * FROM post WHERE comment_id = ?;";
    private static  final String FIND_ALL_BY_POST_ID = "SELECT * FROM comment WHERE post_id = ?;";
    private static  final String FIND_ALL_QUERY = "SELECT * FROM comment;";
    private static  final String DELETE_QUERY = "DELETE FROM WHERE comment.comment_id = ?;";
    private static  final String UPDATE_QUERY= "UPDATE comment SET comment_id = ?, post_id = ?, message = ?, " +
            "author_name, rating = ?, created_at = ? WHERE comment_id = ? ;";

    public void create (Comment comment){
        try {
            con = ConnectionUtils.getConnection();
            st = con.prepareStatement(INSERT_QUERY);

            st.setInt(1, comment.getCommentId());
            st.setInt(2, comment.getPostId());
            st.setString(3, comment.getMessage());
            st.setString(4, comment.getAuthorName());
            st.setInt(5, comment.getRating());
            st.setDate(6, comment.getCreatedAt());

            int savedRows = st.executeUpdate();
            System.out.println("CommentRepository -> Saved + savedRows + comment(s)");
        }catch (Exception ex){
            ex.printStackTrace();
            throw new RuntimeException("Error on create!", ex);
        }finally{
            ConnectionUtils.close(con,st,rs);
        }

    }
    public Comment findById(int id) {
        try {
            con = ConnectionUtils.getConnection();
            st = con.prepareStatement(FIND_BY_ID_QUERY);
            st.setInt(1, id);
            rs = st.executeQuery();
            Comment foundComment = extract(rs);
            System.out.println("Found comment: " + foundComment);
            return foundComment;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException("Error", ex);
        } finally {
            ConnectionUtils.close(con, st, rs);
        }
    }
    public List<Comment> findAll() {
        try {
            con = ConnectionUtils
                    .getConnection();

            st = con.prepareStatement(FIND_ALL_QUERY);
            rs = st.executeQuery();

            List<Comment> foundComments = extractAll(rs);
            System.out.println(foundComments.size() + "comments were found");

            return foundComments;
        } catch (SQLException ex){
            ex.printStackTrace();
            throw new RuntimeException("Error", ex);
        } finally {

            ConnectionUtils.close(con,st,rs);
        }
    }
    public void update(Comment comment) throws SQLException {
        con = ConnectionUtils.getConnection();

        st = con.prepareStatement(UPDATE_QUERY);
        st.setInt(1, comment.getCommentId());
        st.setInt(2, comment.getPostId());
        st.setString(3, comment.getMessage());
        st.setString(4, comment.getAuthorName());
        st.setInt(5, comment.getRating());
        st.setDate(6, comment.getCreatedAt());

        int updateRows = st.executeUpdate();
        System.out.println(updateRows + " order(s) was update");

        ConnectionUtils.close(con,st,rs);
    }
    public void deleteById(int id) throws SQLException {
        con =  ConnectionUtils.getConnection();

        st = con.prepareStatement(DELETE_QUERY);
        st.setInt(1,id);

        int deletedRows = st.executeUpdate();
        System.out.println(deletedRows + " comment(s) was deleted");

        ConnectionUtils.close(con,st,rs);
    }

    public  List<Comment> findAllByPostId(int postId) throws Exception{
        try {
            con = ConnectionUtils
                    .getConnection();
            st = con.prepareStatement(FIND_ALL_BY_POST_ID);
            rs = st.executeQuery();

            List<Comment> foundComments = extractAll(rs);
            System.out.println(foundComments.size() + "comments were found");

            return foundComments;
        } catch (SQLException ex){
            ex.printStackTrace();
            throw new RuntimeException("Error", ex);
        } finally {

            ConnectionUtils.close(con,st,rs);
        }

    }

    private Comment extract(ResultSet rs) throws SQLException {
        if (rs.next()) {
            Comment comment = new Comment();
            comment.setCommentId(rs.getInt("comment_id"));
            comment.setPostId(rs.getInt("post_id"));
            comment.setMessage(rs.getString("message"));
            comment.setAuthorName(rs.getString("author_name"));
            comment.setRating(rs.getInt("rating"));
            comment.setCreatedAt(rs.getDate("created_at"));
            return comment;
        }
        return null;
    }

    private List<Comment> extractAll(ResultSet rs) throws SQLException{
        List<Comment> foundComments = new ArrayList<>();

        while (rs.next()){
            int commentId = rs.getInt("comment_id");
            int postId = rs.getInt("post_id");
            String message = rs.getString("message");
            String authorName = rs.getString("author_name");
            int rating = rs.getInt("rating");
            Date createdAt = rs.getDate("created_at");
            Comment comment = new Comment(commentId, postId, message,  authorName, rating, createdAt);
            foundComments.add(comment);
        }
        return foundComments;
    }

}
