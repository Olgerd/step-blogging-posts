package by.itstep.blogging.repository;

import by.itstep.blogging.entity.Post;
import by.itstep.blogging.utils.ConnectionUtils;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PostRepository {

    private Connection con;
    private PreparedStatement st;
    private ResultSet rs;

    private static  final String INSERT_QUERY = "INSERT INTO post(post_id,title,description,rating,author_name) " +
           "VALUES (?,?,?,?,?);";
    private static  final String FIND_BY_ID_QUERY = "SELECT * FROM post WHERE post_id = ?;";
    private static  final String FIND_ALL_QUERY = "SELECT * FROM post;";
    private static  final String DELETE_QUERY = "DELETE FROM WHERE post.post_id = ?;";
    private static  final String UPDATE_QUERY= "UPDATE post SET post_id = ?, title = ?, description = ?, rating = ?, " +
       " author_name = ? WHERE post_id = ? ;";

    public void create (Post post){
        try {
            con = ConnectionUtils.getConnection();
            st = con.prepareStatement(INSERT_QUERY);

            st.setInt(1, post.getPostId());
            st.setString(2,post.getTitle());
            st.setString(3,post.getDescription());
            st.setInt(4,post.getRating());
            st.setString(5,post.getAuthorName());

            int savedRows = st.executeUpdate();
            System.out.println("PostRepository -> Saved + savedRows + post(s)");
        }catch (Exception ex){
            ex.printStackTrace();
            throw new RuntimeException("Error on create!", ex);
        }finally{
            ConnectionUtils.close(con,st,rs);
        }

    }
    public Post findById(int id) {
        try {
            con = ConnectionUtils.getConnection();
            st = con.prepareStatement(FIND_BY_ID_QUERY);
            st.setInt(1, id);
            rs = st.executeQuery();
            Post foundPost = extract(rs);
            System.out.println("Found post: " + foundPost);
            return foundPost;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new RuntimeException("Error", ex);
        } finally {
            ConnectionUtils.close(con, st, rs);
        }
    }
    public List<Post> findAll() {
        try {
            con = ConnectionUtils
                    .getConnection();

            st = con.prepareStatement(FIND_ALL_QUERY);
            rs = st.executeQuery();

            List<Post> foundPosts = extractAll(rs);
            System.out.println(foundPosts.size() + "posts were found");

            return foundPosts;
        } catch (SQLException ex){
            ex.printStackTrace();
            throw new RuntimeException("Error", ex);
        } finally {

            ConnectionUtils.close(con,st,rs);
        }
    }
    public void update(Post post) throws SQLException {
        con = ConnectionUtils.getConnection();

        st = con.prepareStatement(UPDATE_QUERY);
        st.setInt(1, post.getPostId());
        st.setString(2,post.getTitle());
        st.setString(3,post.getDescription());
        st.setInt(4,post.getRating());
        st.setString(5,post.getAuthorName());

        int updateRows = st.executeUpdate();
        System.out.println(updateRows + " order(s) was update");

        ConnectionUtils.close(con,st,rs);
    }

    public void deleteById(int id) throws SQLException {
        con =  ConnectionUtils.getConnection();

        st = con.prepareStatement(DELETE_QUERY);
        st.setInt(1,id);

        int deletedRows = st.executeUpdate();
        System.out.println(deletedRows + " post(s) was deleted");

        ConnectionUtils.close(con,st,rs);
    }

    private Post extract(ResultSet rs) throws SQLException {
        if (rs.next()) {
            Post post = new Post();

            post.setPostId(rs.getInt("post_id"));
            post.setTitle(rs.getString("title"));
            post.setDescription(rs.getString("description"));
            post.setRating(rs.getInt("rating"));
            post.setAuthorName(rs.getString("author_name"));
            return post;
        }
        return null;
    }
    private List<Post> extractAll(ResultSet rs) throws SQLException{
        List<Post> foundPosts = new ArrayList<>();

        while (rs.next()){
            int postId = rs.getInt("post_id");
            String title = rs.getString("title");
            String description = rs.getString("description");
            int rating = rs.getInt("rating");
            String authorName = rs.getString("author_name");
            Post post = new Post(postId, title, description, rating, authorName);
            foundPosts.add(post);
        }
        return foundPosts;
    }

}
