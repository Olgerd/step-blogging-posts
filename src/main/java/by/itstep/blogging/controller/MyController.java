package by.itstep.blogging.controller;

import by.itstep.blogging.entity.Comment;
import by.itstep.blogging.entity.Post;
import by.itstep.blogging.repository.CommentRepository;
import by.itstep.blogging.repository.PostRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class MyController {

    private PostRepository postRepository = new PostRepository();
    private CommentRepository commentRepository = new CommentRepository();

    @GetMapping("/index") // -> localhost:8080/index
    public String getIndex(Model model) {
        try {
            List<Post> foundPosts = postRepository.findAll();
            System.out.println("Found:");
            System.out.println(foundPosts);
            model.addAttribute("foundPosts", foundPosts);
            return "index";
        } catch (Exception ex) {
            ex.printStackTrace();
            return "error";
        }
    }

    @GetMapping("/post-comments")
    public String getPostComments(Model model) {
        try{
        System.out.println("ID:" );
        List<Post> foundPosts = postRepository.findAll();
        List<Comment> foundComments = commentRepository.findAll();
        model.addAttribute("foundComments",foundComments);
        model.addAttribute("foundPosts",foundPosts);

        Comment commentToCreate = new Comment();
        model.addAttribute("commentToCreate",commentToCreate);

        return "post-comments";
        } catch (Exception ex) {
            ex.printStackTrace();
            return "error";
        }
    }
    @PostMapping("/post-comments")
    public String createComment(Comment comment){
        try{
            commentRepository.create(comment);
            return "create-post";
        }catch (Exception ex){
            ex.printStackTrace();
            return "error";
        }
    }


    @GetMapping("/create-post")
    public String getPost(Model model) {

        try {
            List<Post> foundPosts = postRepository.findAll();
            System.out.println("Found:");
            System.out.println(foundPosts);
            model.addAttribute("foundPosts", foundPosts);

            Post postToCreate = new Post();
            model.addAttribute("postToCreate",postToCreate);

            return "create-post";
        } catch (Exception ex) {
            ex.printStackTrace();
            return "error";
        }
    }
    @PostMapping("/create-post")
    public String createPost(Post post){
        try{
            postRepository.create(post);
            return "create-post";
        }catch (Exception ex){
            ex.printStackTrace();
            return "error";
        }
    }
}
